'use strict';

/**
 * @ngdoc overview
 * @name myproyectsApp
 * @description
 * # myproyectsApp
 *
 * Main module of the application.
 */
var app = angular
  .module('myproyectsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl'
      })
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when('/startproject', {
        templateUrl: 'views/startproject.html',
        controller: 'StartprojectCtrl'
      })
      .when('/porjectlist', {
        templateUrl: 'views/porjectlist.html',
        controller: 'PorjectlistCtrl'
      })
      .when('/callslist', {
        templateUrl: 'views/callslist.html',
        controller: 'CallslistCtrl'
      })
      .when('/myprojects', {
        templateUrl: 'views/myprojects.html',
        controller: 'MyprojectsCtrl'
      })
      .when('/oneproject', {
        templateUrl: 'views/oneproject.html',
        controller: 'OneprojectCtrl'
      })
      .when('/config', {
        templateUrl: 'views/config.html',
        controller: 'ConfigCtrl'
      })
      .when('/notifications', {
        templateUrl: 'views/notifications.html',
        controller: 'NotificationsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

app.factory('UserService', function() {
    return {
        name : 'abeljimo',
        project : 'web-programing-front',
        lat : 46.0452644, 
        longi : 14.4767646,
        server : 'https://arcane-chamber-8130.herokuapp.com',
        id_project_selected : 0,
        login : 0,
        user : null,
        other_user : null
    };
});


