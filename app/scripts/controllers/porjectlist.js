'use strict';
/*global $:false */

/**
 * @ngdoc function
 * @name myproyectsApp.controller:PorjectlistCtrl
 * @description
 * # PorjectlistCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('PorjectlistCtrl', function ($scope, $location, $http, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    if (UserService.login!=1){
      $location.path('/login');
    }

    $scope.go = function ( path ) {
      $location.path( path );
    };

    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'block';

    $('.remove').remove();
    var li = $('<li> Project List </li>');
    li.addClass('active');
    li.addClass('remove');
    $('.breadcrumb').append(li);

    $scope.items = [];


    if (UserService.user.role == 0){

      $http.get(UserService.server+"project/")
        .then(function (response) {
          $scope.items = response.data;
          $scope.selected = $scope.items[0];
          UserService.id_project_selected = $scope.items[0].id_project;

          if(UserService.server+'user/'+UserService.user.id_user+'/' == $scope.items[0].designer){
            UserService.other_user = $scope.items[0].developer
          }
          else if(UserService.server+'user/'+UserService.user.id_user+'/' == $scope.items[0].developer){ 
            UserService.other_user = $scope.items[0].designer
          }

          $('#info_pre').text("----------------------\nid_project : "+$scope.items[0].id_project+"\nname : "+$scope.items[0].name+"\ndeadline : "+$scope.items[0].deadline
        +"\nposition : "+$scope.items[0].position+"\ncreatedD : "+$scope.items[0].createdD+"\n----------------------\n");
        });
    }
    else {

      $http.get(UserService.server+"project_user/?id_user="+UserService.user.id_user)
        .then(function (response) {
          $scope.items = response.data;

          if($scope.items.length <= 0){
          alert("Sorry, you don't have projects actually");
          $location.path('/dashboard');
          }
          
          $scope.selected = $scope.items[0];
          UserService.id_project_selected = $scope.items[0].id_project;

          if(UserService.server+'user/'+UserService.user.id_user+'/' == $scope.items[0].designer){
            UserService.other_user = $scope.items[0].developer
          }
          else if(UserService.server+'user/'+UserService.user.id_user+'/' == $scope.items[0].developer){ 
            UserService.other_user = $scope.items[0].designer
          }

          $('#info_pre').text("----------------------\nid_project : "+$scope.items[0].id_project+"\nname : "+$scope.items[0].name+"\ndeadline : "+$scope.items[0].deadline
        +"\nposition : "+$scope.items[0].position+"\ncreatedD : "+$scope.items[0].createdD+"\n----------------------\n");
        });
    }
    



    $scope.select= function(item,index) {
      $scope.selected = item;
      $scope.elem_index = index;
      UserService.id_project_selected = item.id_project;

      if(UserService.server+'user/'+UserService.user.id_user+'/' == item.designer){
        UserService.other_user = item.developer
      }
      else if(UserService.server+'user/'+UserService.user.id_user+'/' == item.developer){ 
        UserService.other_user = item.designer
      }
      

     $('#info_pre').text("----------------------\nid_project : "+item.id_project+"\nname : "+item.name+"\ndeadline : "+item.deadline
      +"\nposition : "+item.position+"\ncreatedD : "+item.createdD+"\n----------------------\n");

    };

    $scope.isActive = function(item) {
      return $scope.selected === item;
    };


    $scope.remove_prpject = function () {
    if (confirm('Do you wan\'t to delete this item?')) {
          $scope.items.splice($scope.elem_index, 1);
          $http.delete(UserService.server+"project/"+UserService.id_project_selected+"/");
      }
    };







  });
