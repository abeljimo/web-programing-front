'use strict';
/*global $:false */

/**
 * @ngdoc function
 * @name myproyectsApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('LoginCtrl', function ($scope, $location, $http, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'none';


    $scope.submitForm = function(isValid) {
    if (isValid) {
        
        $http.get(UserService.server+"login/?email="+ $('#email').val() +'&password='+ $('#password').val())
        .then(function (response) {

          if (response.data.length == 1){
            //login
            UserService.login = 1;
            UserService.user = response.data[0];
            $location.path('/dashboard');
          }
          else{
            //Error
            alert('Error in Login');
          }

        });

    }
    else {
      alert('Please insert valid email \nExample : test@test.com \n and Password');
    }
    };

    $scope.go = function ( path ) {
      $location.path( path );
    };



  });
