'use strict';
/*global $:false */

/**
 * @ngdoc function
 * @name myproyectsApp.controller:StartprojectCtrl
 * @description
 * # StartprojectCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('StartprojectCtrl', function ($scope, $http, $location, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    if (UserService.login!=1){
      $location.path('/login');
    }

    if(UserService.user.role != 0){
      $location.path('/dashboard');
    }

  $scope.items = [];
  
  $scope.addItem = function(item){
    if (item != undefined && item != null){
    $scope.items.push(item);
    $scope.newItem = null;
    }
  }
  
  $scope.removeItem = function(index){
    $scope.items.splice(index, 1);
  }

    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'block';

    var li = $('<li> Start Project </li>');
    li.addClass('active');
    li.addClass('remove');
    $('.breadcrumb').append(li);

    $scope.inputpos = UserService.lat + ',' + UserService.longi;

    $scope.data_dev = { repeatSelect: null, availableOptions: []};
    $http.get(UserService.server+"user_role/?role=1")
      .then(function (response) {
        $scope.data_dev.availableOptions = response.data;
      });

    $scope.data_des = { repeatSelect: null, availableOptions: []};
    $http.get(UserService.server+"user_role/?role=2")
      .then(function (response) {
        $scope.data_des.availableOptions = response.data;
      });




    $scope.save = function() {

      if ($scope.items.length > 0 && $('#name').val() && $('#deadline').val() && $('#developer').val() && $('#designer').val()){

        console.log('Abel Abel');
        console.log($('#developer').val());

        var data = { 'name' : $('#name').val() ,'deadline' : $('#deadline').val() , 'position' : $('#posi').val() , 'designer' : UserService.server+"user/"+$('#designer').val()+"/" , 'developer' : UserService.server+"user/"+$('#developer').val()+"/" };
        
        $http.post(UserService.server+"project/",data)
            .then(function (data, status, headers) {

            for(var i = 0; i < $scope.items.length; i++){

              console.log($scope.items[i]);
              var call_data = { 'id_project' : UserService.server+"project/"+data.data.id_project+"/" ,'jsoncontent' : '' , 'name' : $scope.items[i] };
              console.log(call_data);
              $http.post(UserService.server+"call/",call_data)
              .success(function (data, status, headers) {console.log('Call Created');})
            }

            //$location.path('/projectlist');
        })
      }
      else {
        alert('please do not leave empty fields');
      }

    }


  });

