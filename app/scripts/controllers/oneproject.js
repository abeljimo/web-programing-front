'use strict';

/**
 * @ngdoc function
 * @name myproyectsApp.controller:OneprojectCtrl
 * @description
 * # OneprojectCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('OneprojectCtrl', function ($scope, $location, $http, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];



    if (UserService.login!=1){
      $location.path('/login');
    }

    $scope.disabled = false;
    if (UserService.user.role == 0){
        $scope.disabled= true;
    }

    $scope.items = [];


    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'block';

    $('.remove').remove();
    var li = $('<li> Project </li>');
    var li2 = $('<li> <a href="#/porjectlist"> Project List </a> </li>');
    li.addClass('active');
    li.addClass('remove');
    li2.addClass('remove');
    $('.breadcrumb').append(li2);
    $('.breadcrumb').append(li);


    $http.get(UserService.server+"project/"+UserService.id_project_selected)
      .then(function (response) {

        console.log('Vo vo vo');
        console.log(response.data);

        if(UserService.server+'user/'+UserService.user.id_user+'/' == response.data.designer){
          UserService.other_user = response.data.developer
        }
        else if(UserService.server+'user/'+UserService.user.id_user+'/' == response.data.developer){ 
          UserService.other_user = response.data.designer
        }
      });

    $http.get(UserService.server+"get_call/?id_project="+UserService.id_project_selected)
    .then(function (response) {
        $scope.items = response.data;
        $scope.selected = $scope.items[0];    
        $('#json-renderer2').text($scope.items[0].jsoncontent);
    });

	$scope.select= function(item) {
		$scope.selected = item;
        $('#json-renderer2').text(item.jsoncontent);
        $('#json-textarea').text(item.jsoncontent); 
	};

	$scope.isActive = function(item) {
		return $scope.selected === item;
	};

    $scope.editItem = function(index){ 

        if ($('#json-renderer2').is(":visible")){
            $('#json-renderer2').hide();
            $('#json-textarea').show();
            $('#button-textarea').show();
        } 
    }

    $scope.change = function() {

        var data = { 'id_project' : UserService.server+"project/"+UserService.id_project_selected+"/" ,'jsoncontent' : $('#json-textarea').val()};

        $http.put(UserService.server+"call/"+$scope.selected.id_call+"/",data)
            .success(function (data, status, headers) {

                $('#json-renderer2').text($('#json-textarea').val());

                var data = {'id_call' : UserService.server+"call/"+$scope.selected.id_call+"/" , 'id_user' : UserService.other_user };
                
                console.log('Abel data');
                console.log(data);


                $http.post(UserService.server+"notification/",data)
                .then(function (data, status, headers) {
                })

                $http.get(UserService.server+"get_call/?id_project="+UserService.id_project_selected)
                .then(function (response) { 
                    $scope.items = response.data;
                    $('#json-textarea').text(" ");
                });

            })

        if ($('#json-textarea').is(":visible")){
            $('#json-textarea').hide();
            $('#button-textarea').hide();
            $('#json-renderer2').show();
        }
    }


  });
