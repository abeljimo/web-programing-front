'use strict';

/**
 * @ngdoc function
 * @name myproyectsApp.controller:NotificationsCtrl
 * @description
 * # NotificationsCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('NotificationsCtrl', function ($scope, $location, $http, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    if (UserService.login!=1){
      $location.path('/login');
    }

    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'block';

    $('.remove').remove();
    var li = $('<li> Notifications </li>');
    li.addClass('active');
    li.addClass('remove');
    $('.breadcrumb').append(li);

	$scope.items = [];

  $http.get(UserService.server+"get_noti/?id_user="+UserService.user.id_user)
  .then(function (response) {
      console.log(response.data);
      $scope.items = response.data;
      $scope.selected = $scope.items[0];
  });

  $scope.select= function(item) {
    $scope.selected = item; 
  };

  $scope.isActive = function(item) {
    return $scope.selected === item;
  };

  $scope.removeItem = function(index,item){
      if (confirm('Do you wan\'t to delete this item?')) {
        $http.delete(UserService.server+"notification/"+item.id_notification+"/");
        $scope.items.splice(index, 1);
    }
  }

  $scope.go = function (item) {
    UserService.id_project_selected = item.id_project
    $location.path('oneproject');
  };


  });
