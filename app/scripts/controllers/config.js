'use strict';

/**
 * @ngdoc function
 * @name myproyectsApp.controller:ConfigCtrl
 * @description
 * # ConfigCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('ConfigCtrl', function ($scope, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'block';

    $('.remove').remove();
    var li = $('<li> Configuration </li>');
    li.addClass('active');
    li.addClass('remove');
    $('.breadcrumb').append(li);

    $scope.name = UserService.name;
    $scope.project = UserService.project;

    var lat;
    var longi;


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        $('#savpos').text(UserService.lat +','+ UserService.longi);
    }



   function showPosition(position) {
        lat = position.coords.latitude
        longi = position.coords.longitude; 
        var latlon = lat +','+ longi
        $('#currpos').text(latlon);
        var img_url = "http://maps.googleapis.com/maps/api/staticmap?center="
        +latlon+"&zoom=11&size=200x200&sensor=true";
        $('#mapholder').append("<img src='"+img_url+"'>");
    }

    $scope.updatepos = function() {
      console.log(lat);

      UserService.lat = lat;
      UserService.longi = longi;

      console.log(UserService.lat);

    };


  });
