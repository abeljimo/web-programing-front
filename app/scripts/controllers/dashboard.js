'use strict';
/*global $:false */

/**
 * @ngdoc function
 * @name myproyectsApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('DashboardCtrl', function ($scope, $location, $http, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    if (UserService.login!=1){
      $location.path('/login');
    }


    $scope.buttondisable = false;
    if (UserService.user.role == 0){
        $scope.buttondisable= true;
    }



    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    $('.remove').remove();

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'block';

    $scope.go = function ( path ) {
      $location.path( path );
    };

    $http.get("https://bitbucket.org/api/2.0/repositories/"+UserService.name+"/"+UserService.project+"/commits/")
    .then(function (response) {
      for (var i = 0; i < response.data.values.length; i++) {

        var li = $('<span class="ligitlog">'+
          '<p>commit: '+ response.data.values[i].hash +'</br>'+
          'Author: '+ response.data.values[i].author.raw +'</br>'+
          'Date: '+ response.data.values[i].date +'</br>'+
          '</br> <font color="#AEB404">'+ response.data.values[i].message +'</font></p>'+ 
          +' </span>');


        $('#realgitlog').append(li);
      }   
    });



  });
