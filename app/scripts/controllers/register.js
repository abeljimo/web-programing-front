'use strict';

/**
 * @ngdoc function
 * @name myproyectsApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the myproyectsApp
 */
angular.module('myproyectsApp')
  .controller('RegisterCtrl', function ($scope, $location, $http, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var navbar = document.getElementById('mynavbar');
    navbar.style.display = 'none';

    var navbar2 = document.getElementById('mynavbar2');
    navbar2.style.display = 'none';

    $scope.submitForm = function(isValid) {
    if (isValid) {

        var data = { 'name' : $('#name').val() ,'email' : $('#email').val() , 'password' : $('#password').val(), country : $('#country').val() , role : $('#role').val() };

        $http.post(UserService.server+"user/",data)
        .then(function (data, status, headers) {
          alert('Registration completed');
          UserService.login = 1;
          UserService.user = data.data;
          $location.path('/dashboard');
        })

    }
    else {
      alert('Please insert valid email and name');
    }
    };

    $scope.go = function ( path ) {
      $location.path( path );
    };


  });
